package data

type Object struct {
	Prototype        *Object
	StringProperties map[String]Property
	SymbolProperties map[*Symbol]Property
	Call             func(Value, ...Value) (Value, Value)
	Construct        func([]Value, *Object) (*Object, Value)
}

type Property struct {
	Enumerable   bool
	Configurable bool
	Value        Value
	Writable     bool
	Getter       *Object
	Setter       *Object
}

func (o *Object) Type() string {
	if o.Call != nil {
		return "function"
	} else {
		return "object"
	}
}

func (*Object) ToNumber() (Number, Value) {
	panic("NYI")
}

func (o *Object) ToString() (String, Value) {
	panic("NYI")
}
