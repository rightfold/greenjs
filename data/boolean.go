package data

type Boolean bool

func (Boolean) Type() string {
	return "boolean"
}

func (b Boolean) ToNumber() (Number, Value) {
	if b {
		return 1, nil
	} else {
		return 0, nil
	}
}

func (b Boolean) ToString() (String, Value) {
	if b {
		return "true", nil
	} else {
		return "false", nil
	}
}
