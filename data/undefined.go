package data

import "math"

type Undefined struct{}

func (Undefined) Type() string {
	return "undefined"
}

func (Undefined) ToNumber() (Number, Value) {
	return Number(math.NaN()), nil
}

func (Undefined) ToString() (String, Value) {
	return "undefined", nil
}
