package data

import (
	"math"
	"testing"
)

func isSame(a, b float64) bool {
	return (math.IsNaN(a) && math.IsNaN(b)) || a == b
}

func TestType(t *testing.T) {
	test := func(value Value, expectedType string) {
		if actualType := value.Type(); actualType != expectedType {
			t.Errorf("%v had type %s but %s was expected", actualType, expectedType)
		}
	}

	test(Undefined{}, "undefined")
	test(Null{}, "object")
	test(Boolean(false), "boolean")
	test(Number(0), "number")
	test(String(""), "string")
	test(&Symbol{Name: ""}, "symbol")
	test(&Object{}, "object")
	function := &Object{Call: func(Value, ...Value) (Value, Value) { return nil, nil }}
	test(function, "function")
	test(&Object{Prototype: function}, "object")
}

func TestToNumber(t *testing.T) {
	test := func(value Value, expectedNumber Number) {
		actualNumber, exception := value.ToNumber()
		if exception != nil {
			t.Errorf("ToNumber() threw exception %v", exception)
		} else if !isSame(float64(actualNumber), float64(expectedNumber)) {
			t.Errorf("%v converted to %f but %f was expected", value, actualNumber, expectedNumber)
		}
	}

	test(Undefined{}, Number(math.NaN()))
	test(Null{}, 0)
	test(Boolean(false), 0)
	test(Boolean(true), 1)
	test(Number(0), 0)
	// TODO: String
	// TODO: *Symbol
	// TODO: *Object
}

func TestToString(t *testing.T) {
	test := func(value Value, expectedString String) {
		actualString, exception := value.ToString()
		if exception != nil {
			t.Errorf("ToString() threw exception %v", exception)
		} else if actualString != expectedString {
			t.Errorf("%v converted to %s but %s was expected", value, actualString, expectedString)
		}
	}

	test(Undefined{}, "undefined")
	test(Null{}, "null")
	test(Boolean(false), "false")
	test(Boolean(true), "true")
	// TODO: Number
	test(String(""), "")
	// TODO: *Symbol
	// TODO: *Object
}

func TestToInteger(t *testing.T) {
	test := func(value Value, expectedNumber Number) {
		actualNumber, exception := ToInteger(value)
		if exception != nil {
			t.Errorf("ToNumber() threw exception %v", exception)
		} else if !isSame(float64(actualNumber), float64(expectedNumber)) {
			t.Errorf("%v converted to %f but %f was expected", value, actualNumber, expectedNumber)
		}
	}

	test(Undefined{}, 0)
	test(Null{}, 0)
	test(Boolean(false), 0)
	test(Boolean(true), 1)
	test(Number(0), 0)
	test(Number(math.Inf(-1)), Number(math.Inf(-1)))
	test(Number(math.Inf(1)), Number(math.Inf(1)))
	// TODO: String
	// TODO: *Symbol
	// TODO: *Object
}
