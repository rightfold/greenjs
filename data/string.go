package data

type String string

func (String) Type() string {
	return "string"
}

func (s String) ToNumber() (Number, Value) {
	panic("NYI")
}

func (s String) ToString() (String, Value) {
	return s, nil
}
