package data

type Symbol struct {
	Name string
}

func (*Symbol) Type() string {
	return "symbol"
}

func (*Symbol) ToNumber() (Number, Value) {
	panic("NYI")
}

func (*Symbol) ToString() (String, Value) {
	panic("NYI")
}
