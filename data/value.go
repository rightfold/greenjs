package data

import "math"

type Value interface {
	Type() string
	ToNumber() (Number, Value)
	ToString() (String, Value)
}

func ToInteger(value Value) (Number, Value) {
	number, exception := value.ToNumber()
	if exception != nil {
		return 0, exception
	}
	switch {
	case math.IsNaN(float64(number)):
		return 0, nil
	case number == Number(0) || math.IsInf(float64(number), 0):
		return number, nil
	default:
		return Number(math.Trunc(float64(number))), nil
	}
}
