package data

type Null struct{}

func (Null) Type() string {
	return "object"
}

func (Null) ToNumber() (Number, Value) {
	return 0, nil
}

func (Null) ToString() (String, Value) {
	return "null", nil
}
