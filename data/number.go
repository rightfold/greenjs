package data

type Number float64

func (Number) Type() string {
	return "number"
}

func (n Number) ToNumber() (Number, Value) {
	return n, nil
}

func (Number) ToString() (String, Value) {
	panic("NYI")
}
